##the following section is for developers requiring the testing pod to be instantiated with a volume mounted on your host directory 
tag?=0.0.2
IMAGE_TO_TEST=gerhardlr/pano-testing-image:$(tag)
PANO-HOSTNAME := $(shell cat ../../server-side/base-arch/shared_values.yaml | shyaml get-value pano-hostname)
location:= $(shell cd ../../ && pwd)
kube_path ?= $(shell echo ~/.kube)
k8_path ?= $(shell echo ~/.minikube)
THIS_HOST := $(shell (ip a 2> /dev/null || ifconfig) | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
KUBE_NAMESPACE=ptna
USER = node
testing-pod?=pano-testing-pod
#the port mapping to host
hostPort ?= 3000
hostSshPort ?= 2040
testing-config := '{ "apiVersion": "v1","spec":{\
					"containers":[{\
						"image":"$(IMAGE_TO_TEST)",\
						"name":"testing-container",\
						"volumeMounts":[{\
							"mountPath":"/home/$(USER)/panopticon/",\
							"name":"testing-volume"}],\
						"ports":[{\
							"containerPort":3000,\
							"hostPort":$(hostPort)},{\
							"containerPort":22,\
							"hostPort":$(hostSshPort)}]}],\
					"volumes":[{\
						"name":"testing-volume",\
						"hostPath":{\
							"path":"$(location)",\
							"type":"Directory"}}]}}'



deploy_testing_pod: 
	@kubectl run $(testing-pod) \
		--image=$(IMAGE_TO_TEST) \
		--namespace $(KUBE_NAMESPACE) \
		--wait \
		--generator=run-pod/v1 \
		--overrides=$(testing-config)
	@kubectl wait --for=condition=Ready pod/$(testing-pod)
	@kubectl exec -it $(testing-pod) -- bash -c "mkdir /home/$(USER)/.ssh && touch authorized_keys && chown $(USER):$(USER) -R /home/$(USER)/.ssh/"
	@kubectl exec -it $(testing-pod) -- bash -c "echo '$(THIS_HOST) $(PANO-HOSTNAME)' >> /etc/hosts"
	

	
delete_testing_pod:
	@kubectl delete pod $(testing-pod) --namespace $(KUBE_NAMESPACE)

attach_testing_pod:
	@kubectl exec -it $(testing-pod) --namespace $(KUBE_NAMESPACE) /bin/bash

bake_testing_image:
	docker build -t $(IMAGE_TO_TEST) . 

push_testing_image:
	docker push $(IMAGE_TO_TEST)
docker_run:
	docker run --rm --name temp -d -p 2030:22 $(IMAGE_TO_TEST) 
	docker exec -it temp bash

docker_delete:
	docker stop temp

ssh_config = "Host kube-host\n\tHostName $(THIS_HOST) \n \tUser ubuntu"

test_as_ssh_client:
	@kubectl exec -it $(testing-pod) -- bash -c "ssh-keygen -t rsa -f /home/$(USER)/.ssh/id_rsa -q -P ''"
	@kubectl exec -it $(testing-pod) -- bash -c "cat /home/$(USER)/.ssh/id_rsa.pub" >>~/.ssh/authorized_keys
	@kubectl exec -it $(testing-pod) -- bash -c "chown $(USER):$(USER) -R /home/$(USER)/.ssh/"
	@echo $(ssh_config) >temp
	@kubectl cp temp $(KUBE_NAMESPACE)/$(testing-pod):/home/$(USER)/.ssh/config
	@rm temp

